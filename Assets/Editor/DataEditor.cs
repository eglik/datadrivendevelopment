﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Data), editorForChildClasses:true)]
public class DataEditor : Editor
{
    Data data;
    private void OnEnable()
    {
        data = target as Data;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if(GUILayout.Button("Update"))
        {
            data.UpdateData();
        }
    }
}
