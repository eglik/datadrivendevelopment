﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class ControllerFinder : EditorWindow
{
    class SearchedObject
    {
        public BaseController controller;
        public bool isNeedBinding;
    }
    List<SearchedObject> searchedObjects;

    [MenuItem("Window/Controller Finder %`")]
    public static void ShowFinder()
    {
        var cf = GetWindow<ControllerFinder>();
        cf.searchedObjects = new List<SearchedObject>();
    }

    static bool CheckBindingValid(object c)
    {
        var fields = c.GetType().GetFields(System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
        foreach (var f in fields)
        {
            var atts = f.GetCustomAttributes(true);
            foreach (var atr in atts)
            {
                if (atr is ModelAttribute)
                {
                    if (f.GetValue(c) == null)
                    {
                        return false;
                    }
                }
            }
        }
        return true;
    }
    private void OnGUI()
    {
        var controllers = GameObject.FindObjectsOfType<BaseController>();
        if(searchedObjects.Count != controllers.Length)
        {
            searchedObjects.Clear();
            foreach (var c in controllers)
            {
                searchedObjects.Add(new SearchedObject() { controller = c });
            }
        }

        foreach (var c in searchedObjects)
        {
            c.isNeedBinding = !CheckBindingValid(c.controller);

            using (new EditorGUILayout.HorizontalScope())
            {
                EditorGUILayout.LabelField(string.Format("{0}[{1}]", c.controller.name, c.controller.GetType().Name));
                if(c.isNeedBinding)
                {
                    if(GUILayout.Button("Binding Data"))
                    {
                        var fields = BaseViewEditor.DetectModel(c.controller);
                        BaseViewEditor.BindingModel(c.controller, fields);
                    }
                }
                if (GUILayout.Button("Select"))
                {
                    Selection.activeGameObject = c.controller.gameObject;
                }
            }
        }
    }
}
