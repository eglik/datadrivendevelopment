﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(Data), true)]
public class DataPropertyDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);
            if(property.objectReferenceValue == null)
            {
                EditorGUILayout.LabelField("[Model] " + property.name + "할당 필요");
            }
            else
            {
                EditorGUILayout.LabelField("[Model] " + property.name + " 할당됨");
            }
        EditorGUI.EndProperty();
    }
}
