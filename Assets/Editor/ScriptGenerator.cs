﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using System.Reflection;
using System.IO;

public class ScriptGenerator : EditorWindow
{
    string scriptName = "New Controller";
    enum GenerateType { Controller, Data };
    GenerateType gType;

    [MenuItem("Assets/Create/Script/NewController",priority = 100)]
    public static void CreateControllerEditor()
    {
        var editor = EditorWindow.GetWindow<ScriptGenerator>();
        editor.gType = GenerateType.Controller;
        
    }

    class UseModelProperty
    {
        public Type type;
        public bool isUse;
        public string fieldName;
    }
    List<UseModelProperty> dataTypes;
    private void OnEnable()
    {
        dataTypes = new List<UseModelProperty>();

        Type[] types = typeof(Data).Assembly.GetTypes();
        foreach (var type in types)
        {
            if(type.IsSubclassOf(typeof(Data)))
            {
                dataTypes.Add(new UseModelProperty() { type = type, isUse = false, fieldName = "" });
            }
        }
    }

    Vector2 scrollView = new Vector2(0,0);
    private void OnGUI()
    {
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("컨트롤러 이름", GUILayout.MaxWidth(80));
        scriptName = EditorGUILayout.TextField(scriptName);
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.Space();

        GUILayout.Label("사용할 데이터", new GUIStyle() { fontSize = 12, margin = new RectOffset(5,0,5,5) } );
        scrollView = EditorGUILayout.BeginScrollView(scrollView);
        foreach (var d in dataTypes)
        {

            EditorGUILayout.BeginHorizontal();
            d.isUse = EditorGUILayout.Toggle(d.isUse, GUILayout.MaxWidth(10));
            GUILayout.Label(d.type.Name, GUILayout.MaxWidth(100));
            if(d.isUse)
            {
                GUILayout.Label("필드명:", GUILayout.MaxWidth(50));
                d.fieldName = GUILayout.TextField(d.fieldName);
            }
            EditorGUILayout.EndHorizontal();

        }
        EditorGUILayout.EndScrollView();

        if (GUILayout.Button("생성"))
        {
            GenerateScript();
        }
    }

    private void GenerateScript()
    {
        string path;
        var obj = Selection.activeObject;
        if (obj == null) path = "Assets";
        else path = AssetDatabase.GetAssetPath(obj.GetInstanceID());

        if(File.Exists(path))
        {
            path = Path.GetDirectoryName(path);
        }

        //  var resultPath = EditorUtility.SaveFilePanel("Generating Controller script...", path, scriptName, ".cs");

        var t = AssetDatabase.LoadAssetAtPath<TextAsset>("Assets/Editor/Resources/ControllerTemplate.cs.txt");

        string property = "";
        string binding = "";
        foreach (var p in dataTypes)
        {
            if(p.isUse)
            {
                if (p.fieldName == "")
                {
                    Debug.LogError("비어있는 필드값이 있습니다.");
                    return;
                }
                property += string.Format("[SerializeField, Model] private {0} {1}; \r\n\t", p.type.Name, p.fieldName);
                binding += string.Format("UseData({0}); \r\n\t\t", p.fieldName);
            }
        }
        string script = t.text;

        var sn = MakeValidFileName(scriptName).Replace(" ","");
        script = script.Replace("${ViewName}", sn);
        script = script.Replace("${Model}", property);
        script = script.Replace("${Binding}", binding);

        File.WriteAllText(Path.Combine(path, MakeValidFileName(scriptName) + ".cs"), script);

        AssetDatabase.Refresh();
    }

    private string MakeValidFileName(string name)
    {
        string invalidChars = System.Text.RegularExpressions.Regex.Escape(new string(System.IO.Path.GetInvalidFileNameChars()));
        string invalidRegStr = string.Format(@"([{0}]*\.+$)|([{0}]+)", invalidChars);

        return System.Text.RegularExpressions.Regex.Replace(name, invalidRegStr, "");
    }
}
