﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Reflection;
using UnityEditor.SceneManagement;

[CustomEditor(typeof(BaseController), editorForChildClasses:true)]
public class BaseViewEditor : Editor
{
    BaseController view;

    List<FieldInfo> modelField = new List<FieldInfo>();

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        bool isNull = false;
        if(modelField.Count == 0)
        {
            EditorGUILayout.LabelField(target.name + "에 할당된 모델이 없습니다. 스크립트에서 Model 객체를 만들어주세요.");
        }
        else
        {
            foreach (var f in modelField)
            {
                if (f.GetValue(target) == null)
                {
                    isNull = true;
                }
            }

            if(isNull)
            {
                if(GUILayout.Button("Binding"))
                {
                    BindingModel(target,modelField);
                    EditorUtility.SetDirty(target);
                    EditorSceneManager.MarkAllScenesDirty();
                }
            }
        }

    }
    private void OnEnable()
    {
        view = this.target as BaseController;
        modelField = DetectModel(view);
    }

    public static void BindingModel(object target, List<FieldInfo> fieldInfos)
    {
        FindObjectOfType<Store>().Initialize();
        foreach (var t in fieldInfos)
        {
            if (t.GetValue(target) == null)
            {
                t.SetValue(target, Store.GetData(t.FieldType));
            }
        }
    }

   
    public static List<FieldInfo> DetectModel(BaseController controller)
    {
        List<FieldInfo> modelField = new List<FieldInfo>();

        var t = controller.GetType();

        var fields = t.GetFields(BindingFlags.Instance | BindingFlags.NonPublic);

        foreach (var field in fields)
        {
            var attrs = field.GetCustomAttributes(true);
            foreach (var a in attrs)
            {
                if (a is ModelAttribute)
                {
                    modelField.Add(field);
                }
            }
        }

        return modelField;
    }
}
