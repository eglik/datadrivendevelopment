﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public class ModelAttribute : Attribute { }

public abstract  class BaseController : UIBinder
{

    Action unbindingEvent;
    
    protected virtual void OnDestroy()
    {
        unbindingEvent();
    }
    protected void UseData(Data d)
    {
        d = Store.GetData(d.GetType());
        d.Binding(this);

        unbindingEvent = () =>
        {
            d.UnBinding(this);
        };
    }
    public abstract void UpdateData();
}






