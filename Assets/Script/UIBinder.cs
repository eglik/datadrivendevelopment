﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;


public class UIBinding : Attribute
{
    public string name;
    public UIBinding(string n)
    {
        name = n;
    }
}


public class UIBinder : MonoBehaviour
{
    bool isBinded;
    protected virtual void Awake()
    {
        if(!isBinded) BindingUI();
    }
    protected void BindingUI()
    {
        isBinded = true;

        var t = this.GetType();

        var fields = t.GetFields(BindingFlags.Instance | BindingFlags.NonPublic);


        foreach (var field in fields)
        {

            var attrs = field.GetCustomAttributes(true);
            foreach (var a in attrs)
            {
                if (a is UIBinding)
                {
                    var attribute = a as UIBinding;

                    var type = field.FieldType;

                    var comps = this.GetComponentsInChildren(type);
                    foreach (var comp in comps)
                    {
                        if (comp.name == attribute.name)
                        {
                            field.SetValue(this, comp);
                        }
                    }
                }
            }
        }
    }
}