﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Data : MonoBehaviour
{
    Action onUpdateData;

    public void Binding(BaseController v)
    {
        onUpdateData += v.UpdateData;
        v.UpdateData();
    }
    public void UnBinding(BaseController v)
    {
        onUpdateData -= v.UpdateData;
    }

    public void UpdateData()
    {
        if (onUpdateData != null)
        {
            onUpdateData();
        }
    }
}
