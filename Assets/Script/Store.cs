﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[ExecuteAlways]
public class Store : MonoBehaviour
{
    List<Data> dataList = new List<Data>();
    private static Store instance;
    

    private void Awake()
    {
        if(instance != null && instance != this)
        {
            DestroyImmediate(gameObject);
            return;
        }
        else
        {
            Initialize();
        }
    }

    public void Initialize()
    {
        instance = this;
        dataList.Clear();
        var comps = GetComponents<Data>();
#if UNITY_EDITOR
        Debug.Log("[Store] 초기화 : " + comps.Length + "개의 데이터 모델 인식");
#endif
        dataList.AddRange(comps);
    }

    public static T GetData<T>() where T : Data
    {
        return instance.dataList.Find((d) => { return (d is T); }) as T;
    }

    public static Data GetData(Type t)
    {
        return instance.dataList.Find((d) => { return (d.GetType() == t); });
    }
}
