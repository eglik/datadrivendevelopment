using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectedHeroTitle : BaseController
{
	[SerializeField, Model] private SelectedHero selectedHero;

    [UIBinding("title")]
    Text title;

    private void Start()
    {
		UseData(selectedHero); 
		
    }

    public override void UpdateData()
    {
        //여기에서 모델이 변경될때마다의 처리를 하십시오.
        if(selectedHero.hero.name != "")
        {
            title.text = selectedHero.hero.name;
        }
    }
    
}
