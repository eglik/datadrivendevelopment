using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeroList : BaseController
{
	[SerializeField, Model] private HeroData heroList; 

    [SerializeField]
    GameObject menuItemPrefab;
    [SerializeField]
    Transform menuRoot;

    private void Start()
    {
		UseData(heroList);
    }

    public override void UpdateData()
    {
        for(int i = 0; i < menuRoot.childCount; i++)
        {
            Destroy(menuRoot.GetChild(i).gameObject);
        }

        foreach(var h in heroList.heroList)
        {
            var item = Instantiate(menuItemPrefab, menuRoot).GetComponent<HeroMenuItem>();
            item.SetHero(h);
        }
    }
}