using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeroDetail : BaseController
{
	[SerializeField, Model] private SelectedHero hero;

    [UIBinding("name")]
    Text nameText;
    [UIBinding("portrait")]
    Image portrait;
    [UIBinding("grade")]
    Text grade;
    [UIBinding("story")]
    Text story;

    private void Start()
    {
		UseData(hero); 	
    }

    public override void UpdateData()
    {
        gameObject.SetActive(hero.hero.name != "");
        nameText.text = hero.hero.name;
        portrait.sprite = hero.hero.portrait;
        grade.text = hero.hero.grade.ToString();
        story.text = hero.hero.story;
    }
}
