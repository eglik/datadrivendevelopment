﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LibraryData : Data
{
    public string title;
    public string desc;
    public Sprite mainImage;
}
