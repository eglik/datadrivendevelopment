using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TourHeroList : BaseController
{
	[SerializeField, Model] private TourHeroData heroList;

    [SerializeField]
    private GameObject listPrefab;
    [SerializeField]
    private Transform listRootTrans;

    [SerializeField]
    private InputField inputField;

    [SerializeField]
    private GameObject[] topHeros;

    private void Start()
    {
		UseData(heroList); 
    }

    public override void UpdateData()
    {
        for (int i = 0; i < listRootTrans.childCount; i++)
        {
            Destroy(listRootTrans.GetChild(i).gameObject);
        }

        for (int i = 0; i < heroList.heroList.Count; i++)
        {
            var item = Instantiate(listPrefab, listRootTrans).GetComponent<TourHeroItem>();
            item.SetData(heroList.heroList[i], i);
        }

        List<TourHero> sortList = new List<TourHero>(heroList.heroList);

        Debug.Log(sortList.Count);

        sortList.Sort(delegate (TourHero A, TourHero B)
        {
            return -A.score.CompareTo(B.score);
        });

        foreach(var topObj in topHeros)
        {
            topObj.SetActive(false);
        }

        for (int i = 0; i < Mathf.Min(sortList.Count, 4); i++)
        {
            topHeros[i].SetActive(true);
            topHeros[i].GetComponentInChildren<Text>().text = sortList[i].name;
        }
    }
    
    public void AddUser()
    {
        heroList.heroList.Add(new TourHero(inputField.text));
        inputField.text = "";
        UpdateData();
    }
}