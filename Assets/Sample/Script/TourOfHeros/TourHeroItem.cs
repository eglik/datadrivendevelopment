﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TourHeroItem : MonoBehaviour
{
    [SerializeField]
    private Text name;
    private int index;

    public void SetData(TourHero hero, int index)
    {
        name.text = string.Format("[{0}] {1}", index, hero.name);
        this.index = index;
    }

    public void DeleteData()
    {
        Store.GetData<TourHeroData>().heroList.RemoveAt(index);
        Store.GetData<TourHeroData>().UpdateData();
    }
}