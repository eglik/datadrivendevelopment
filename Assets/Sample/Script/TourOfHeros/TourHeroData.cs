﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TourHero
{
    public string name;
    public int score;

    public TourHero(string name)
    {
        this.name = name;
    }
}

public class TourHeroData : Data
{
    public List<TourHero> heroList;
}