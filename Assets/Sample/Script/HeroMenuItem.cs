﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeroMenuItem : MonoBehaviour
{
    public Button button;
    public Image portrait;
    public Text nameText;

    public void SetHero(Hero hero)
    {
        portrait.sprite = hero.portrait;
        nameText.text = hero.name;

        button.onClick.AddListener(new UnityEngine.Events.UnityAction(() => {
            var sh = Store.GetData<SelectedHero>();
            sh.hero = hero;
            sh.UpdateData();
        }));
    }

}
