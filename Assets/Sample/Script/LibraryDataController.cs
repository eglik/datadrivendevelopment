using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LibraryDataController : BaseController
{
	[SerializeField, Model] private LibraryData data;

    [UIBinding("title")]
    Text title;
    [UIBinding("description")]
    Text desc;
    [UIBinding("mainImage")]
    Image mainImage;

    private void Start()
    {
		UseData(data); 
		
    }

    public override void UpdateData()
    {
        title.text = "[����]" + data.title;
        desc.text = data.desc;
        mainImage.sprite = data.mainImage;
        StartCoroutine(FadeInOnChange());
    }  


    IEnumerator FadeInOnChange()
    {
        for(float t = 0; t <= 1 + Time.deltaTime; t += Time.deltaTime)
        {
            Color c = Color.Lerp(new Color(0, 0, 0, 0), Color.black,t);
            title.color = c;
            desc.color = c;
            mainImage.color = Color.Lerp(new Color(1, 1, 1, 0), Color.white, t);
            yield return null;
        }
    }

}
