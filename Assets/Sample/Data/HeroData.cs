﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum HeroGrade { C, B , A, S, SS,SSS}
[System.Serializable]
public class Hero
{
    public string name;
    public HeroGrade grade;
    public Sprite portrait;
    public string story;
}

public class HeroData : Data
{
    public List<Hero> heroList;
}
